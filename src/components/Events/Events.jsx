import React from 'react';
import moment from 'moment';
import propTypes from 'prop-types';

import DataEvents from '../../DataStructure/DataEvents';

import './Events.sass';

const Events = props => {
  const { selectedDay } = props;

  return DataEvents.map((event, index) => {
    if (selectedDay !== moment(event.date).format('YYYY-MM-DD')) {
      return '';
    }
    return (
      <div className="PromptItem" key={ index }>
        <div className="event-date">
          {moment(event.date).format('ddd, D MMM')}
        </div>
        {event.events.map((e, ind) => (
          <div
            className={ `prompt ${ moment(event.date).isBefore(moment().format('YYYY-MM-DD'))
                ? 'past'
                : ''
            }` }
            key={ ind }
          >
            <div className="wrapper-header">
              <div className="event-header">{e.name}</div>
              <div className="event-header time">
                {moment(e.time).format('LT')}
              </div>
            </div>
            <div className="event-body">{e.body}</div>
          </div>
        ))}
      </div>
    );
  });
};

Events.propTypes = {
  selectedDay: propTypes.string.isRequired
};

export default Events;
