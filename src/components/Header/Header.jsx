import React from 'react';
import moment, { isMoment } from 'moment';
import propTypes from 'prop-types';

import './Header.sass';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseShow: false
    };
  }

  Collaps() {
    this.setState({ collapseShow: !this.state.collapseShow });
  }

  weekData() {
    const { collapseShow } = this.state;
    const { startPeriod, endPeriod } = this.props;
    return (
      <div className="main-layer">
        <div
          className="navigation"
          onClick={ () => this.props.setPeriod('-1', 'week') }
        >
          prev
        </div>
        <div className="switch-button" onClick={ this.Collaps.bind(this) }>
          <div className="collaps">
            {moment(startPeriod).format('MMMM') +
              ' ' +
              moment(startPeriod).format('D') +
              '-' +
              moment(endPeriod).format('D')}
          </div>
          <div className={ `pointer ${ collapseShow ? 'inverted' : '' }` } />
        </div>
        <div
          className="navigation"
          onClick={ () => this.props.setPeriod('1', 'week') }
        >
          next
        </div>
      </div>
    );
  }

  monthData() {
    const { collapseShow } = this.state;
    const { startPeriod } = this.props;
    return (
      <div className="main-layer">
        <div
          className="navigation"
          onClick={ () => this.props.setPeriod('-1', 'month') }
        >
          {moment(startPeriod)
            .subtract(1, 'months')
            .format('MMM')}
        </div>
        <div className="switch-button" onClick={ this.Collaps.bind(this) }>
          <div className="collaps upper-case">
            {moment(startPeriod).format('MMMM')}
          </div>
          <div className={ `pointer ${ collapseShow ? 'inverted' : '' }` } />
        </div>
        <div
          className="navigation"
          onClick={ () => this.props.setPeriod('1', 'month') }
        >
          {moment(startPeriod)
            .add(1, 'months')
            .format('MMM')}
        </div>
      </div>
    );
  }

  render() {
    const { collapseShow } = this.state;
    const { periodType } = this.props;
    return (
      <div className="Header-wrapper">
        {periodType === 'week' ? this.weekData() : this.monthData()}
        {collapseShow ? (
          <div className="buttons-wrapper">
            <button
              className="buttons-changer"
              onClick={ () => {
                this.props.initialState('week');
                this.Collaps();
              } }
            >
              This week
            </button>
            <button
              className="buttons-changer"
              onClick={ () => {
                this.props.initialState('month');
                this.Collaps();
              } }
            >
              This month
            </button>
          </div>
        ) : null}
        {collapseShow ? (
          ''
        ) : (
          <div className="daysNames">
            <span className="weekDay">S</span>
            <span className="weekDay">M</span>
            <span className="weekDay">T</span>
            <span className="weekDay">W</span>
            <span className="weekDay">T</span>
            <span className="weekDay">F</span>
            <span className="weekDay">S</span>
          </div>
        )}
      </div>
    );
  }
}

Header.propTypes = {
  periodType: propTypes.string.isRequired,
  setPeriod: propTypes.func.isRequired,
  initialState: propTypes.func.isRequired,
  startPeriod: isMoment,
  endPeriod: isMoment
};

export default Header;
