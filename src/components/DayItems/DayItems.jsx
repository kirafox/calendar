import React from 'react';
import moment from 'moment';
import propTypes from 'prop-types';

import './DayItems.sass';

const DayItems = props => {
  const { day, isSelected, underscore } = props;

  let nameDay = false;
  const weekDay = moment(day).format('dddd');
  if (weekDay === 'Saturday' || weekDay === 'Sunday') {
    nameDay = true;
  }

  let isToday = false;
  const currentDay = moment().format('YYYY-MM-DD');
  if (day === currentDay) {
    isToday = true;
  }

  return (
    <div className={ 'DayItems ' } onClick={ () => props.onDayClick(day) }>
      <div className={ `${ isSelected ? 'active' : '' }` }>
        <div className={ ` ${ isToday ? 'today' : '' }` } />
        <div className={ `day-wrapper ${ nameDay ? 'weekends' : ' ' } ` }>
          {moment(day).isValid() ? (
            <div className="day">
              {moment(day).format('DD')}
              <div className="backlight">{underscore}</div>
            </div>
          ) : (
            ' '
          )}
        </div>
      </div>
    </div>
  );
};

DayItems.propTypes = {
  day: propTypes.string.isRequired,
  underscore: propTypes.array.isRequired,
  onDayClick: propTypes.func.isRequired,
  isSelected: propTypes.bool
};

export default DayItems;
