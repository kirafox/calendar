import React, { Component } from 'react';
import Dates from './containers/Dates/Dates';

import './App.sass';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Dates/>
      </div>
    );
  }
}

export default App;
