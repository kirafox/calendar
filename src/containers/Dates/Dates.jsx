import React from 'react';
import moment from 'moment';

import Header from '../../components/Header/Header';
import DayItems from '../../components/DayItems/DayItems';
import Events from '../../components/Events/Events';
import DataEvents from '../../DataStructure/DataEvents';

import './Dates.sass';

class Dates extends React.Component {
  constructor() {
    super();
    this.state = {
      period: [],
      periodType: 'month',
      selectedDay: '',
      startPeriod: '',
      endPeriod: ''
    };

    this.onDayClick = this.onDayClick.bind(this);
    this.setPeriod = this.setPeriod.bind(this);
    this.initialState = this.initialState.bind(this);
  }

  componentDidMount() {
    this.initialState(this.state.periodType);
    this.getSignature();
  }

  initialState(periodType) {
    const startPeriod = moment().startOf(periodType);
    const endPeriod = moment().endOf(periodType);
    this.setState({
      startPeriod,
      endPeriod
    });
    this.createPeriod(startPeriod, endPeriod, periodType);
    const selectedDay = moment().format('YYYY-MM-DD');
    this.setState({ selectedDay, periodType });
  }

  createPeriod(startPeriod, endPeriod, periodType) {
    let start = moment(startPeriod).startOf(periodType);
    const end = moment(endPeriod).endOf(periodType);

    const newPeriod = [];
    while (start <= end) {
      newPeriod.push(start.format('YYYY-MM-DD'));
      start = start.clone().add(1, 'days');
    }

    if (periodType === 'month') {
      const dayNumber = moment(startPeriod)
        .startOf(periodType)
        .format('d');
      for (let i = 0; i < dayNumber; i++) {
        newPeriod.unshift('');
      }
    }
    this.setState({ period: newPeriod });
  }

  onDayClick(day) {
    this.setState({ selectedDay: moment(day).format('YYYY-MM-DD') });
  }

  setPeriod(offset, periodType) {
    let { startPeriod, endPeriod } = this.state;
    startPeriod = startPeriod.clone().add(offset, periodType);
    endPeriod = endPeriod.clone().add(offset, periodType);
    this.setState({ startPeriod, endPeriod });
    this.createPeriod(startPeriod, endPeriod, periodType);
  }

  getSignature() {
    return moment(this.state.selectedDay).format('dddd, D MMMM');
  }

  choseDay(selectedDay, day) {
    if (selectedDay) {
      return selectedDay === day;
    }
    return false;
  }

  backlight(day) {
    const occasion = DataEvents.find(e => moment(e.date).format('YYYY-MM-DD') === day);
    let countEvents = 0;
    if (occasion !== undefined) {
      countEvents = occasion.events.length;
    }
    const mass = [];
    for (let i = 0; i < countEvents && i < 3; i++) {
      mass.push(
        <span
          key={ i }
          className={ `line ${
            moment(day).isBefore(moment().format('YYYY-MM-DD')) ? 'past' : ''
          }` }
        />
      );
    }
    return mass;
  }

  render() {
    const {
      startPeriod,
      endPeriod,
      period,
      periodType,
      selectedDay
    } = this.state;
    return (
      <div>
        <Header
          startPeriod={ startPeriod }
          endPeriod={ endPeriod }
          periodType={ periodType }
          setPeriod={ this.setPeriod }
          initialState={ this.initialState }
        />
        <div className="Dates">
          <div className="dateItem">
            {period.map((day, index) => (
              <DayItems
                key={ index }
                day={ day }
                underscore={ this.backlight(day) }
                onDayClick={ () => this.onDayClick(day) }
                isSelected={ this.choseDay(selectedDay, day) }
              />
            ))}
          </div>
          <div className="dateSignature">{this.getSignature()}</div>
          <div>
            <Events selectedDay={ selectedDay } />
          </div>
        </div>
      </div>
    );
  }
}

export default Dates;
