import moment from "moment";

const DataEvents = [
  {
    date: moment('2019-01-01', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      },
      { name: "practice", body: "blanket's creek", time: moment("11:00:00", "hh:mm:ss") },
      {
        name: "practice",
        body: "Pine Mountain Overlook Loop",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },  
  {
    date: moment('2019-01-16', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-01-21', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      },
      {
        name: "practice",
        body: "blanket's creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-01-31', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-02-01', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      },
      { name: "practice", body: "blanket's creek", time: moment("11:00:00", "hh:mm:ss") },
      {
        name: "practice",
        body: "Pine Mountain Overlook Loop",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-02-02', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      },
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-02-05', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-02-08', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      },
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      },
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-02-18', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-02-21', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      },
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-03-08', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-03-19', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-04-03', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  },
  {
    date: moment('2019-04-05', 'YYYY-MM-DD'),
    events: [
      {
        name: "practice",
        body: "sope creek",
        time: moment("11:00:00", "hh:mm:ss")
      }
    ]
  }
];

export default DataEvents;
