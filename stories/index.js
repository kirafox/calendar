import React from "react";
import { storiesOf } from "@storybook/react";
import Header from "../src/components/Header/Header";
import Dates from "../src/containers/Dates/Dates";
import DayItems from "../src/components/DayItems/DayItems";
import Events from "../src/components/Events/Events";

storiesOf("Calendar", module)
  .add("Header", () => <Header />)
  .add("Dates", () => <Dates />)
  .add("DayItems", () => <DayItems />)
  .add("Events", () => <Events />);
